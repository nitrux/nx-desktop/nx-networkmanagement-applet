# NX Network Management Applet [![Build Status](https://travis-ci.org/nx-desktop/nx-networkmanagement-applet.svg?branch=master)](https://travis-ci.org/nx-desktop/nx-networkmanagement-applet)

This is the repository for the Network Manager used in Nitrux.

![](https://i.imgur.com/NdmVw4x.png)

# Requirements
- Qt 5.8+.
- Qml.
- QtQuick.
- I18n.
- Plasma 5.8.4+.
- WindowSystem.
- KIO.

# Issues
If you find problems with the contents of this repository please create an issue.

©2019 Nitrux Latinoamericana S.C.
